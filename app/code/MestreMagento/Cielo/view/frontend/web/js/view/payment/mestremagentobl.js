define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'mestremagentobl',
                component: 'MestreMagento_Cielo/js/view/payment/method-renderer/mestremagentobl'
            }
        );
        return Component.extend({});
    }
);