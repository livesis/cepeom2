define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';

        if(window.checkoutConfig.payment.mestremagentodc.homologation_mode_enabled) {
            rendererList.push(
                {
                    type: 'mestremagentodc',
                    component: 'MestreMagento_Cielo/js/view/payment/method-renderer/mestremagentodc'
                }
            );
        }

        return Component.extend({});
    }
);