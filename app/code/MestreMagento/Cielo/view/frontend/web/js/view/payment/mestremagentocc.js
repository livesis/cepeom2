define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';

        if(window.checkoutConfig.payment.mestremagentocc.homologation_mode_enabled) {
            rendererList.push(
                {
                    type: 'mestremagentocc',
                    component: 'MestreMagento_Cielo/js/view/payment/method-renderer/mestremagentocc'
                }
            );
        }

        return Component.extend({});
    }
);  