<?php


namespace MestreMagento\Cielo\Cron;

class Cronjob
{

    protected $logger;

    /**
     * Constructor
     *
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(\Psr\Log\LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Execute the cron
     *
     * @return void
     */
    public function execute()
    {

        $from = date('Y-m-d h:i:s', strtotime('-30 day', strtotime(date("Y-m-d h:i:s"))));
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $cielo_status = $objectManager->get('MestreMagento\Cielo\Helper\Data');



        $orderDatamodel = $objectManager->get('Magento\Sales\Model\Order')->getCollection()
            ->setOrder('created_at','DESC')
            ->addFieldToFilter('created_at', array('from'=>$from));

        foreach($orderDatamodel as $orderDatamodel1) {

            $adictionalInfo = $orderDatamodel1->getPayment()->getAdditionalInformation();


            if(isset($adictionalInfo['PaymentId'])){

                $status_cielo = $cielo_status->consultPymentCielo($adictionalInfo['PaymentId']);
                $orderState = '';
                switch ($status_cielo) {
                    case 1:
                    case 2:
                         $orderState = \Magento\Sales\Model\Order::STATE_PROCESSING;
                        break;
                    case 3:
                    case 10:
                    case 11:
                    case 13:
                        $orderState = \Magento\Sales\Model\Order::STATE_CANCELED;
                        break;
                    case 12:
                    case 0:
                        $orderState = \Magento\Sales\Model\Order::STATE_PENDING_PAYMENT;
                        break;
                }

                if($orderState){
                    $orderDatamodel1->setState($orderState)->setStatus($orderState)->save();
                }

            }
        }

    }
}
