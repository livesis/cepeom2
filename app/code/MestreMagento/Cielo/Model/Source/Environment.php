<?php
/**
 *
 * @author      Jamacio Rocha
 * @copyright   2018 MestreMagento (https://mestremagento.com.br)
 * @license     https://mestremagento.com.br Copyright
 *
 * @link        https://mestremagento.com.br/
 */
namespace MestreMagento\Cielo\Model\Source;

class Environment implements \Magento\Framework\Option\ArrayInterface
{
    const SANDBOX = 'sandbox';
    const PRODUCTION = 'production';

    public function toOptionArray()
    {
        return [
            [
                'value' => self::PRODUCTION,
                'label' => __('Production')
            ],
            [
                'value' =>  self::SANDBOX,
                'label' =>__('Sandbox')
            ]
        ];
    }
}
