<?php
/**
 *
 * @author      Jamacio Rocha
 * @copyright   2018 MestreMagento (https://mestremagento.com.br)
 * @license     https://mestremagento.com.br Copyright
 *
 * @link        https://mestremagento.com.br/
 */
namespace MestreMagento\Cielo\Model\Source;

use Magento\Framework\Option\ArrayInterface;
use Magento\Payment\Model\Method\AbstractMethod;

class PaymentAction implements ArrayInterface
{
    public function toOptionArray()
    {
        return [
            [
                'value' => AbstractMethod::ACTION_AUTHORIZE,
                'label' => __('Authorize')
            ],
            [
                'value' =>  AbstractMethod::ACTION_AUTHORIZE_CAPTURE,
                'label' =>__('Authorize and Capture')
            ]
        ];
    }
}
