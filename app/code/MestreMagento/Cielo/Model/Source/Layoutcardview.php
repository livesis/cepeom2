<?php
/**
 *
 * @author      Jamacio Rocha
 * @copyright   2018 MestreMagento (https://mestremagento.com.br)
 * @license     https://mestremagento.com.br Copyright
 *
 * @link        https://mestremagento.com.br/
 */
namespace MestreMagento\Cielo\Model\Source;

use Magento\Framework\Option\ArrayInterface;
use Magento\Payment\Model\Method\AbstractMethod;

class Layoutcardview implements ArrayInterface
{
    public function toOptionArray()
    {
        return [
            [
                'value' => 1,
                'label' => __('Layout default')
            ],
            [
                'value' =>  2,
                'label' =>__('Layout custom')
            ]
        ];
    }
}
