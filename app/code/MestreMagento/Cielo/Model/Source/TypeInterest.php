<?php
/**
 *
 * @author      Jamacio Rocha
 * @copyright   2018 MestreMagento (https://mestremagento.com.br)
 * @license     https://mestremagento.com.br Copyright
 *
 * @link        https://mestremagento.com.br/
 */
namespace MestreMagento\Cielo\Model\Source;

use Magento\Backend\App\Action;

class TypeInterest implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [
            'simple' => __('Simple Interest'),
            'compound' => __('Compound Interest'),
        ];
    }
}