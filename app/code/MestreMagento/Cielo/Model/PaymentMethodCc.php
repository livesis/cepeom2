<?php
namespace MestreMagento\Cielo\Model;

use Magento\Framework\UrlInterface;
use \Magento\Payment\Model\Method\AbstractMethod;
use Magento\Sales\Model\Order;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Sales\Model\Order\Payment;



class PaymentMethodCc extends \Magento\Payment\Model\Method\Cc
{
    const ROUND_UP = 100;
    protected $_canAuthorize = true;
    protected $_canCapture = false;
    protected $_canRefund = true;
    protected $_code = 'mestremagentocc';
    protected $_isGateway               = true;
    protected $_canCapturePartial       = true;
    protected $_canRefundInvoicePartial = true;
    protected $_canVoid                = true;
    protected $_canCancel              = true;
    protected $_canUseForMultishipping = false;
    protected $_countryFactory;
    protected $_supportedCurrencyCodes = ['BRL'];
    protected $_debugReplacePrivateDataKeys = ['number', 'exp_month', 'exp_year', 'cvc'];
    protected $_cart;
    protected $_mestremagentoHelper;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \Magento\Framework\Module\ModuleListInterface $moduleList,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Checkout\Model\Cart $cart,
        \MestreMagento\Cielo\Helper\Data $mestremagentoHelper,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $moduleList,
            $localeDate,
            null,
            null,
            $data
        );
        $this->_countryFactory = $countryFactory;
        $this->scopeConfig = $scopeConfig;
        $this->_cart = $cart;
        $this->_mestremagentoHelper = $mestremagentoHelper;

        $paymentAction = $this->_mestremagentoHelper->getPaymentAction();
        if($paymentAction == 'authorize_capture'){
            $this->_canCapture = true;
        }

    }

    public function assignData(\Magento\Framework\DataObject $data)
    {
        parent::assignData($data);
        $infoInstance = $this->getInfoInstance();
        $currentData = $data->getAdditionalData();
        foreach($currentData as $key=>$value){
            $infoInstance->setAdditionalInformation($key,$value);
        }
        return $this;
    }

    public function validate()
    {
        return $this;
    }

    public function authorize(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {

        try {
            $sale =  $this->_mestremagentoHelper->addPayCreditCard($payment, $amount,'createsale');

            if($this->getCoreConfig2('payment/mestremagentoconfig/environment') == 'production') {
                if (!in_array($sale->getPayment()->getReturnCode(), ['4', '6', '00'])) {
                    throw new \Magento\Framework\Exception\LocalizedException(__($this->getMessageRetorno($sale->getPayment()->getReturnCode())));
                }
            }

            $payment->setTransactionId($payment->getAdditionalInformation('PaymentId') . '-authorization')
                ->setTxnType(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_AUTH)
                ->setIsTransactionClosed(true);

        } catch (\Exception $exception) {
            throw new \Magento\Framework\Exception\LocalizedException(__($exception->getMessage()));
        }

        return $this;


    }
    public function getMessageRetorno($code){

        switch($code){
            case 05:
                return __("Transaction not authorized No Balance Cod: 05");
                break;
            case 57:
                return __("Transaction not authorized Expired Card Cod: 57");
                break;
            case 78:
                return __("Transaction not authorized Card Locked Cod: 78");
                break;
            case 99:
                return __("Transaction not authorized Time Out Cod: 99");
                break;
            case 77:
                return __("Transaction not authorized Canceled Card Cod: 77");
                break;
            case 70:
                return __("Transaction not authorized Credit Card Problems Cod: 70");
                break;
            default;
                return __("Transaction not authorized Credit Card Problems");

        }

    }
    public function capture(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        try {
            $sale = $this->_mestremagentoHelper->addPayCreditCard($payment, $amount, 'capturesale');
            if($this->getCoreConfig2('payment/mestremagentoconfig/environment') == 'production') {
                if (!in_array($sale->getPayment()->getReturnCode(), ['4','6', '00'])) {
                    throw new \Magento\Framework\Exception\LocalizedException(__($this->getMessageRetorno($sale->getPayment()->getReturnCode())));
                }
            }

            $payment->setTransactionId($payment->getAdditionalInformation('PaymentId') . '-capture')
                ->setTxnType(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_CAPTURE)
                ->setIsTransactionClosed(true);

        } catch (\Exception $exception) {
            $this->_mestremagentoHelper->setLog($exception->getMessage());
            throw new \Magento\Framework\Exception\LocalizedException(__($exception->getMessage()));
        }

        return $this;
    }

    public function getCoreConfig2($value){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;
        $scopeConfig = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface');
        return $scopeConfig->getValue($value, $storeScope);
    }

    public function isAvailable(\Magento\Quote\Api\Data\CartInterface $quote = null)
    {
        if (!$this->isActive($quote ? $quote->getStoreId() : null)) {
            return false;
        }
        return true;
    }

}