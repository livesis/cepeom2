<?php
/**
* Copyright © Livesis , Inc. All rights reserved.
* See COPYING.txt for license details.
* version 1.0.18 .
*/

\Magento\Framework\Component\ComponentRegistrar::register(
\Magento\Framework\Component\ComponentRegistrar::THEME,
'frontend/Livesis/tema_bootstrap_4',
__DIR__
);
