var config = {
    paths: {
        'bootstrap':'Magento_Theme/js/bootstrap.bundle.min',
        'jquery-ui':'Magento_Theme/js/jquery-ui.min',
        'master-script':'Magento_Theme/js/master-script'
    },
    shim: {
        'bootstrap': {'deps': ['jquery']},
        'jquery-ui': {'deps': ['jquery']},
        'master-script': {'deps': ['jquery']}
    }
};